#include <stdio.h>
#include <stdlib.h>

#define MAX 5

typedef struct {
	int com, tam, fim;
	int itens[MAX];
}Fila;

void iniciaFila(Fila *F){
	F->com = 2;
	F->tam = 0;
	F->fim = 1;
}

void insereFila(Fila *F, int valor){
	if(F->tam == MAX)
		puts("Fila cheia");
	else{
		F->tam++;
		F->fim++;
		if(F->fim == MAX)
			F->fim = 0;
		F->itens[F->fim] = valor;
	}
}

int retiraFila(Fila *F){
	if(F->tam == 0)
		puts("Fila vazia");
	else{
		F->tam--;
		F->com = (F->com+1)%MAX;
		return F->itens[F->tam];
	}
}

void printTela(Fila *F){
	int i;
	for (i = 0; i<F->com; i++){
		printf("[%d]", F->itens[i]);
	}
	for (i = F->com; i<F->tam; i++){
		printf("[%d]", F->itens[i]);			
	}
	printf("\n");
}

void main(){
	Fila A;
	iniciaFila(&A);
	insereFila(&A, 1);
	insereFila(&A, 2);
	insereFila(&A, 3);
	insereFila(&A, 4);
	insereFila(&A, 5);
	printTela(&A);
	retiraFila(&A);
	printTela(&A);
}